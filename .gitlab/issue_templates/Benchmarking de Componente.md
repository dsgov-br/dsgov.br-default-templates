<!-- As informações a seguir destacam os principais pontos relacionados ao componente. Certifique-se de considerar as diretrizes estabelecidas na V3, bem como as definições já implementadas na V4, garantindo consistência e alinhamento com o Design System. -->

## 1. Informações Gerais do Componente

- **Nome do Componente**: Liste possíveis nomes que já estão sendo utilizados e reconhecidos pelos usuários em componentes com características semelhantes ao que será desenvolvido. É possível criar nomes que façam mais sentido para o Design System. Lembre-se de que o nome final deve estar em inglês.
- **Categoria**: Selecione a categoria que melhor se aplique ao componente. Caso haja mais de uma opção relevante, indique aquelas que façam sentido. Sempre que possível, opte por apenas uma ou destaque a categoria principal entre as alternativas:
  - [ ] Ação
  - [ ] Feedback
  - [ ] Formulário
  - [ ] Navegação
  - [ ] Visualização de Dados
  - [ ] Estrutura e Layout
- **Objetivo**: Por que este componente está sendo criado ou atualizado? Qual problema ele resolve?
- **Profissionais Envolvidos**: Liste os responsáveis por DSG, DEV e A11Y.
- **Tarefas Relacionadas**: Liste as tarefas relacionadas a esta atividade, como aquelas que a originaram ou quaisquer outras conexões que ajudem a compreender melhor o contexto. Inclua os links das issues correspondentes.
- **Decisões gerais**: Decisões específicas do time após a realização da pesquisa.

## 2. Pesquisa de *Design*

### 2.1. Levantamento Inicial

- **Referências Visuais**: Links ou anexos com inspirações e benchmarkings relevantes.
- **Componentes Relacionados**: Liste componentes que apresentem semelhanças visuais ou comportamentais.
- **Tipos**: Liste as possíveis variações do componente e o objetivo de cada uma das variações.
- **Comportamentos**: O que este componente precisa fazer? Liste todos os comportamentos importantes para a documentação. A seguir, estão alguns exemplos de comportamentos que podem ser detalhados, mas não se limite apenas a eles:
  - **Estados**: Quais estados fazem sentido?
  - **Densidade**: Liste os três tipos.
  - **Internacionalização**: Como o componente se comporta em diferentes idiomas? Quais são as considerações específicas para a internacionalização? Existe o comportamento RTL (Right-to-Left) para este componente?
  - **Movimento (*motion*)**: Existe alguma animação específica que deve ser considerada? Quais são as propriedades (transição, atenuação e duração) de movimento que podem ser utilizadas? Existe alguma animação pronta que pode ser reaproveitada?
  - **Skeleton Screen**: Definir a apresentação.
  - **Responsividade**: Apresentação e uso em diferentes dispositivos.
- **Consumo pelo usuário**: Verificar nos [Cases de Sucesso](https://case-sucesso-react-c24a71.gitlab.io/) do Design System como estão consumindo o componente para uma possível evolução de comportamento.
- **Boas Práticas**: Listar possíveis boas práticas.

### 2.2. Anatomia

- Liste todos os elementos que fazem parte da anatomia do componente. Inclua o nome, a referência à documentação do Design System (se aplicável) e a obrigatoriedade de uso (opcional, obrigatório ou condicional).
- Verifique se algum elemento pode se tornar um novo componente do Design System.

### 2.3. Estrutura do Componente

**Wireframe/Rascunho Inicial**: Caso necessário, anexe ou descreva o layout do componente, detalhando os comportamentos mais relevantes.

### 2.4. Decisões de Estilo

- Liste possíveis tokens ou propriedades visuais que possam influenciar decisões.
- Verifique animações (propriedades de movimento) que podem ser aplicadas ao componente.

## 3. Especificação de Desenvolvimento

### 3.1. Levantamento Inicial

- **Configurações**: Ênfases, estados, densidades, tipos, etc.
- **Comportamentos**: Detalhe como acionar um tooltip, abrir uma modal, interagir com collapse/dropdown, uso de skeleton screens, etc;
- **Flexibilidade**: Detalhe se existe uma área de conteúdo livre.
- **Anatomia**: Detalhe as propriedades e subcomponentes.
- **Consumo pelo usuário**: Verifique nos [Cases de Sucesso](https://case-sucesso-react-c24a71.gitlab.io/) do Design System como estão consumindo o componente para uma possível evolução de configuração.
- **Dependências internas**: Liste os componentes usados.
- **Dependências externas**: Liste as bibliotecas ou pacotes de terceiros.
- **API ou Integrações**: Ex.: Necessita de chamadas a uma API?
- **Tag Semântica**: Verifique se existe alguma tag HTML prevista para o componente ou em seus elementos internos.

### 3.2. Anatomia

- Verifique se os elementos listados na anatomia da diretriz "Visão Geral" serão subcomponentes ou propriedades.
- Verifique se é viável que o subcomponente (se for criado) se torne um novo componente do Design System.

## 4. Elementos de Acessibilidade

- **Consumo pelo usuário**: Verificar nos [Cases de Sucesso](https://case-sucesso-react-c24a71.gitlab.io/) do Design System como estão consumindo o componente para uma possível evolução de ajuste de acessibilidade.
- **Compatibilidade com tecnologias assistivas**: É possível implementar o componente de forma acessível? Caso não seja, quais ações podem ser tomadas para corrigir ou minimizar as limitações? Quais cuidados específicos devem ser adotados para garantir a compatibilidade com tecnologias assistivas?
- **Critérios de acessibilidade**: Quais são os requisitos específicos que o componente deve atender para garantir sua acessibilidade? Por exemplo, é necessário verificar as interações relacionadas ao componente, como uso de mouse, teclado, comandos de voz e links, entre outras.
- **Propriedades adicionais**: Existem propriedades adicionais que devem ser implementadas para garantir a acessibilidade? Por exemplo, uma lista precisa informar adequadamente ao usuário o comportamento *collapse* (aberto ou fechado).
- **Considerações críticas**: Quais são as principais considerações relacionadas à acessibilidade que precisam ser avaliadas? Existem alternativas para solucionar possíveis problemas? Por exemplo, o uso de tabelas.  
- **Responsividade**: O componente será responsivo e adaptável a diferentes dispositivos e tamanhos de tela?
- **Uso de HTML semântico e ARIA**: Há tags HTML semânticas adequadas para implementar o componente? Existem recomendações específicas para o uso de ARIA?
- **Instruções para o usuário**: O componente exige instruções claras para facilitar seu uso e é acessível para todos os usuários? Quando um arquivo é disponibilizado, o tipo, por exemplo: DOC, JPG, PDF e o tamanho estão claramente informados? Os rótulos descrevem de forma adequada o uso ou a função do componente?

<!-- Inclua as tags/etiquetas se que aplicam ao contexto desse MR -->

/label ~"categoria::componente" ~"fluxo::analisando" ~"tipo::novo"
