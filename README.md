# Gitlab Configs

Esse repositório contém:

- Templates para issues
- Templates para Merge Requests
- Templates para arquivos
- Configurações de Insights
- Definições das políticas de segurança

> Outras configurações, como regras de push e branches protegidas, não são definidas aqui mas sim nas configurações de cada grupo ou projeto.
